﻿using DocApp.Interfaces.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Interfaces.ViewModels
{
    public interface IPatientViewModel
    {
        IPatientView View { get; }
    }
}
