﻿using DocApp.Interfaces;
using DocApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Services
{
    public class PatientService : IPatientService
    {
        public IRepository Repository { get; }
        public PatientService(IRepository repository)
        {
            Repository = repository;
        }

        public bool Edit(Patient patient)
        {
            return this.Repository.EditPatient(patient);
        }

        public Patient GetPatient(int id)
        {
            return this.Repository.GetPatient(id);
        }

        public IEnumerable<Patient> GetPatients()
        {
            return this.Repository.GetPatients();
        }

        public bool RemovePatient(Patient patient)
        {
            return this.Repository.RemovePatient(patient);
        }

        public Patient Add(Patient patient)
        {
            return this.Repository.AddPatient(patient);
        }
    }
}
