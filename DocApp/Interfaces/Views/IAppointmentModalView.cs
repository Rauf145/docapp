﻿using DocApp.Interfaces.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Interfaces.Views
{
    public interface IAppointmentModalView
    {
        void BindDataContext(IAppointmentModalViewModel context);

    }
}
