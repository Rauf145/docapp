﻿using DocApp.Interfaces.ViewModels;
using DocApp.Interfaces.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DocApp.Views
{
    /// <summary>
    /// Interaction logic for AppointmentModalWindow.xaml
    /// </summary>
    public partial class AppointmentModalWindow : Window, IAppointmentModalView
    {
        public AppointmentModalWindow()
        {
            InitializeComponent();
            this.Owner = App.Current.MainWindow;
        }

        public void BindDataContext(IAppointmentModalViewModel context)
        {
            this.DataContext = context;
        }

        private void Header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Minimize(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
    }
}
