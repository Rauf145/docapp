﻿using DocApp.Common;
using DocApp.Interfaces;
using DocApp.Interfaces.ViewModels;
using DocApp.Interfaces.Views;
using DocApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DocApp.ViewModels.Modals
{
    public class PatientModalViewModel : IPatientModalViewModel, INotifyPropertyChanged
    {
        public IPatientModalView View { get; }
        public Patient Model { get; set; }
        public IEnumerable<GenderType> GenderTypes
        {
            get
            {
                return Enum.GetValues(typeof(GenderType)).Cast<GenderType>();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public PatientModalViewModel(IPatientModalView view)
        {
            this.View = view;
            this.View.BindDataContext(this);
            this.Model = new Patient() { Birthday = DateTime.Now };
        }

        #region Commands
        private ICommand save;
        public ICommand Save
        {
            get
            {
                return save ?? (save = new RelayCommand(
                    (execute) =>
                    {
                        (this.View as Window).Close();
                    },
                    (canExecute) =>
                    {
                        return String.IsNullOrEmpty(this.Model?.Error);
                    }
                    ));
            }
        }

        private ICommand close;

        public ICommand Close
        {
            get
            {
                return close ?? (close = new RelayCommand(
                    (execute) =>
                    {
                        this.Model = null;
                        (this.View as Window).Close();
                    }
                    ));
            }
        }

        #endregion

        #region Methods
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
    }
}
