﻿using Autofac;
using DocApp.Common;
using DocApp.Interfaces.ViewModels;
using DocApp.Interfaces.Views;
using DocApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DocApp.ViewModels
{
    public class MainViewModel : IMainViewModel, INotifyPropertyChanged
    {
        public IMainView View { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public MainViewModel(IMainView view)
        {
            this.View = view;
            this.View.BindDataContext(this);
            this.Menu = false;
            this.SelectedIndex = 0;
        }
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private bool menu;

        public bool Menu
        {
            get { return menu; }
            set
            {
                menu = value;
                OnPropertyChanged();
            }
        }

        private UserControl currentView;

        public UserControl CurrentView
        {
            get
            {
                return currentView;
            }
            set
            {
                currentView = value;
                OnPropertyChanged();
            }
        }

        private int selectedIndex;

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                selectedIndex = value;
                switch (selectedIndex)
                {
                    case 0:
                        this.CurrentView = App.Container.Resolve<IAppointmentViewModel>()
                                                        .View as UserControl;
                        break;
                    case 1:
                        this.CurrentView = App.Container.Resolve<IPatientViewModel>()
                                                        .View as UserControl;
                        break;
                }
                OnPropertyChanged();
            }
        }
        private ICommand menuBtnClick;

        public ICommand MenuClick
        {
            get
            {
                return menuBtnClick ?? (menuBtnClick = (new RelayCommand(
                    obj =>
                    {
                        this.Menu = !this.Menu;
                    })
                ));
            }
        }

        private ICommand shutdown;

        public ICommand ShutDown
        {
            get
            {
                return shutdown ?? (shutdown = (new RelayCommand(
                    obj =>
                    {
                        Application.Current.Shutdown();
                    })
                ));
            }
        }

    }
}
