﻿using DocApp.Interfaces.Views;

namespace DocApp.Interfaces.ViewModels
{
    public interface IMainViewModel
    {
        IMainView View { get; }

    }
}