﻿using DocApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Interfaces
{
    public interface IRepository
    {
        #region Appointments
        IEnumerable<Appointment> GetAppointments();
        Appointment GetAppointment(int id);
        Appointment AddAppointment(Appointment appointment);
        bool EditAppointment(Appointment appointment);
        bool RemoveAppointment(Appointment appointment);

        #endregion

        #region Patients
        IEnumerable<Patient> GetPatients();
        Patient GetPatient(int id);
        Patient AddPatient(Patient patient);
        bool EditPatient(Patient patient);
        bool RemovePatient(Patient patient);
        
        #endregion
    }
}
