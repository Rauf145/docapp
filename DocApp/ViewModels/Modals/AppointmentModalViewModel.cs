﻿using DocApp.Common;
using DocApp.Interfaces;
using DocApp.Interfaces.ViewModels;
using DocApp.Interfaces.Views;
using DocApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DocApp.ViewModels.Modals
{
    public class AppointmentModalViewModel : IAppointmentModalViewModel, INotifyPropertyChanged
    {
        public IAppointmentModalView View { get; }
        public Appointment Model { get; set; }
        public IEnumerable<AppointmentType> AppointmentTypes
        {
            get
            {
                return Enum.GetValues(typeof(AppointmentType)).Cast<AppointmentType>();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public AppointmentModalViewModel(IAppointmentModalView view)
        {
            this.View = view;
            this.View.BindDataContext(this);

            this.Model = new Appointment() { Date = DateTime.Now };

        }

        #region Commands
        private ICommand save;
        public ICommand Save
        {
            get
            {
                return save ?? (save = new RelayCommand(
                    (execute) =>
                    {
                        (this.View as Window).Close();
                    },
                    (canExecute) =>
                    {
                        return String.IsNullOrEmpty(this.Model?.Error);
                    }
                    ));
            }
        }

        private ICommand close;

        public ICommand Close
        {
            get
            {
                return close ?? (close = new RelayCommand(
                    (execute) =>
                    {
                        this.Model = null;
                        (this.View as Window).Close();
                    }
                    ));
            }
        }

        #endregion

        #region Methods
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
    }
}