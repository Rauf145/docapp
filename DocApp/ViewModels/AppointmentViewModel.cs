﻿using Autofac;
using DocApp.Common;
using DocApp.Interfaces;
using DocApp.Interfaces.ViewModels;
using DocApp.Interfaces.Views;
using DocApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DocApp.ViewModels
{
    public class AppointmentViewModel : IAppointmentViewModel, INotifyPropertyChanged
    {

        #region Properties

        public IAppointmentView View { get; set; }
        public Window ModalView { get; set; }
        private IAppointmentService Service { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<Appointment> appointments;

        public ObservableCollection<Appointment> Appointments
        {
            get { return appointments; }
            set
            {
                appointments = value;
                OnPropertyChanged();
            }
        }

        public int SelectedIndex { get; set; } = -1;

        private Appointment selectedAppointment;

        public Appointment SelectedAppointment
        {
            get { return selectedAppointment; }
            set
            {
                selectedAppointment = value;
                OnPropertyChanged();
            }
        }


        #endregion
        public AppointmentViewModel(IAppointmentView view, IAppointmentService service)
        {
            this.View = view;
            this.View.BindDataContext(this);

            this.Service = service;
            var appointments = this.Service.GetAppointments();
            if (appointments != null)
                this.Appointments = new ObservableCollection<Appointment>(appointments);
            else
                this.Appointments = new ObservableCollection<Appointment>();
        }



        #region Commands

        private ICommand remove;

        public ICommand Remove
        {
            get
            {
                return remove ?? (remove = new RelayCommand(
                    (obj) =>
                    {
                        var result = this.Service.RemoveAppointment(this.SelectedAppointment);
                        if (result)
                        {
                            this.Appointments.Remove(this.SelectedAppointment);
                            this.SelectedAppointment = null;
                        }
                    },
                    (obj) =>
                    {
                        return this.SelectedAppointment != null;
                    }
                    ));
            }
        }

        private ICommand addModal;

        public ICommand AddModal
        {
            get
            {
                return addModal ?? (addModal = new RelayCommand(
                    (obj) =>
                    {
                        this.ModalView = App.Container.Resolve<IAppointmentModalViewModel>().View as Window;
                        this.ModalView.Closing += AppointmentAddModalClosing;
                        this.ModalView.ShowDialog();
                    }
                    ));
            }
        }

        private ICommand editModal;

        public ICommand EditModal
        {
            get
            {
                return editModal ?? (editModal = new RelayCommand(
                    (obj) =>
                    {//bad code
                        var viewmodel = App.Container.Resolve<IAppointmentModalViewModel>();
                        var model = new Appointment();

                        model.UpdateEntity(this.SelectedAppointment);
                        viewmodel.Model = model;

                        this.ModalView = viewmodel.View as Window;
                        this.ModalView.Closing += AppointmentEditModalClosing;
                        this.ModalView.ShowDialog();
                    },
                    (canExecute) =>
                    {
                        return this.SelectedAppointment != null;
                    }
                    ));
            }
        }
        #endregion;

        #region Methods
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        private void AppointmentAddModalClosing(object sender, EventArgs e)
        {
            var model = (this.ModalView.DataContext as IAppointmentModalViewModel).Model;
            if (model != null)
            {
                var entity = this.Service.Add(model);
                if (entity != null)
                    this.Appointments.Add(entity);
            }
        }

        private void AppointmentEditModalClosing(object sender, EventArgs e)
        {
            var model = (this.ModalView.DataContext as IAppointmentModalViewModel).Model;
            if (model != null)
            {
                var result = this.Service.Edit(model);
                if (result)
                    this.Appointments[this.SelectedIndex] = model;
            }
        }
        #endregion

    }
}
