namespace DocApp
{
    using DocApp.Models;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DocDb : DbContext
    {
        public DocDb()
            : base("DbConnection")
        {
        }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Appointment> Appointments{ get; set; }

    }

}