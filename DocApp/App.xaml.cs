﻿using Autofac;
using DocApp.Interfaces;
using DocApp.Interfaces.ViewModels;
using DocApp.Interfaces.Views;
using DocApp.Services;
using DocApp.ViewModels;
using DocApp.ViewModels.Modals;
using DocApp.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace DocApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static IContainer Container { get; private set; }

        public App()
        {
            var builder = new ContainerBuilder();

            #region Services
            builder.RegisterType<MsSqlRepository>().As<IRepository>();
            builder.RegisterType<AppointmentService>().As<IAppointmentService>();
            builder.RegisterType<PatientService>().As<IPatientService>();
            #endregion

            #region ViewsAndViewModels
            builder.RegisterType<MainWindow>().As<IMainView>();
            builder.RegisterType<MainViewModel>().As<IMainViewModel>();
            builder.RegisterType<PatientView>().As<IPatientView>();
            builder.RegisterType<PatientViewModel>().As<IPatientViewModel>();
            builder.RegisterType<AppointmentView>().As<IAppointmentView>();
            builder.RegisterType<AppointmentViewModel>().As<IAppointmentViewModel>();
            #endregion

            #region Modals
            builder.RegisterType<PatientModalWindow>().As<IPatientModalView>();
            builder.RegisterType<PatientModalViewModel>().As<IPatientModalViewModel>();
            builder.RegisterType<AppointmentModalWindow>().As<IAppointmentModalView>();
            builder.RegisterType<AppointmentModalViewModel>().As<IAppointmentModalViewModel>();
            #endregion

            Container = builder.Build();
        }
    }
}
