﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using DocApp.Common;

namespace DocApp.Models
{
    public class Patient : ValidationEntity<Patient>
    {
        [Key]
        public int Id { get; set; }


        [Required(ErrorMessage ="Name required")]
        [MaxLength(100, ErrorMessage ="maxlength = 100")]
        public string Name { get; set; }


        [Required(ErrorMessage ="Surname required")]
        [MaxLength(100, ErrorMessage = "maxlength = 100")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Patronymic required")]
        [MaxLength(100, ErrorMessage = "maxlength = 100")]
        public string Patronymic { get; set; }


        [Required(ErrorMessage = "Gender required")]
        public GenderType Gender { get; set; }


        [Required(ErrorMessage = "Birthday required")]
        public DateTime Birthday { get; set; }


        [MaxLength(255, ErrorMessage = "maxlength = 255")]
        public string Address { get; set; }


        [MaxLength(100, ErrorMessage = "maxlength = 100")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage ="Incorrect phone number")]
        public string Phone { get; set; }

        public bool UpdateEntity(Patient patient)
        {
            try
            {
                var props = this.GetType()
                                .GetProperties(System.Reflection.BindingFlags.DeclaredOnly |
                                               System.Reflection.BindingFlags.Public |
                                               System.Reflection.BindingFlags.Instance);

                foreach (var prop in props)
                {
                        var newValue = patient.GetType()
                                                 .GetProperty(prop.Name)
                                                 .GetValue(patient);
                        prop.SetValue(this, newValue);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
