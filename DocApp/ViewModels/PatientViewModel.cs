﻿using Autofac;
using DocApp.Common;
using DocApp.Interfaces;
using DocApp.Interfaces.ViewModels;
using DocApp.Interfaces.Views;
using DocApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DocApp.ViewModels
{
    public class PatientViewModel : IPatientViewModel, INotifyPropertyChanged
    {

        #region Properties
        public IPatientView View { get; set; }
        public Window ModalView { get; set; }
        private IPatientService Service { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<Patient> patients;

        public ObservableCollection<Patient> Patients
        {
            get { return patients; }
            set
            {
                patients = value;
                OnPropertyChanged();
            }
        }
        public int SelectedIndex { get; set; } = -1;

        private Patient selectedPatient;

        public Patient SelectedPatient
        {
            get { return selectedPatient; }
            set
            {
                selectedPatient = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public PatientViewModel(IPatientView view, IPatientService service)
        {
            this.View = view;
            this.View.BindDataContext(this);

            this.Service = service;

            var patients = this.Service.GetPatients();
            if (patients != null)
                this.Patients = new ObservableCollection<Patient>(patients);
            else
                this.Patients = new ObservableCollection<Patient>();
        }


        #region Commands

        private ICommand remove;

        public ICommand Remove
        {
            get
            {
                return remove ?? (remove = new RelayCommand(
                    (obj) =>
                    {
                        var result = this.Service.RemovePatient(this.SelectedPatient);
                        if (result)
                        {
                            this.Patients.Remove(this.SelectedPatient);
                            this.SelectedPatient = null;
                        }
                    },
                    (obj) =>
                    {
                        return this.SelectedPatient != null;
                    }
                    ));
            }
        }

        private ICommand addModal;

        public ICommand AddModal
        {
            get
            {
                return addModal ?? (addModal = new RelayCommand(
                    (obj) =>
                    {
                        this.ModalView = App.Container.Resolve<IPatientModalViewModel>().View as Window;
                        this.ModalView.Closing += PatientAddModalClosing;
                        this.ModalView.ShowDialog();
                    }
                    ));
            }
        }

        private ICommand editModal;

        public ICommand EditModal
        {
            get
            {
                return editModal ?? (editModal = new RelayCommand(
                    (obj) =>
                    {//not clean code
                        var viewmodel = App.Container.Resolve<IPatientModalViewModel>();
                        var model = new Patient();

                        model.UpdateEntity(this.SelectedPatient);
                        viewmodel.Model = model;

                        this.ModalView = viewmodel.View as Window;
                        this.ModalView.Closing += PatientEditModalClosing;
                        this.ModalView.ShowDialog();
                    },
                    (canExecute) =>
                    {
                        return this.SelectedPatient != null;
                    }
                    ));
            }
        }

        #endregion;

        #region Methods
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        private void PatientAddModalClosing(object sender, EventArgs e)
        {
            var model = (this.ModalView.DataContext as IPatientModalViewModel).Model;
            if (model != null)
            {
                var entity = this.Service.Add(model);
                if (entity != null)
                    this.Patients.Add(entity);
            }
        }

        private void PatientEditModalClosing(object sender, EventArgs e)
        {
            var model = (this.ModalView.DataContext as IPatientModalViewModel).Model;
            if (model != null)
            {
                var result = this.Service.Edit(model);
                if (result)
                    this.Patients[this.SelectedIndex] = model;
            }
        }
        #endregion
    }
}
