namespace DocApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GenderType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Patients", "Gender", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Patients", "Gender", c => c.Boolean(nullable: false));
        }
    }
}
