﻿using DocApp.Interfaces.ViewModels;
using DocApp.Interfaces.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DocApp.Views
{
    /// <summary>
    /// Interaction logic for PatientView.xaml
    /// </summary>
    public partial class PatientView : UserControl, IPatientView
    {
        public PatientView()
        {
            InitializeComponent();
        }

        public void BindDataContext(IPatientViewModel context)
        {
            this.DataContext = context;
        }
    }
}
