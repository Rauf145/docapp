namespace DocApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDataAnnotationsToPatient : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Patients", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Patients", "Surname", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Patients", "Patronymic", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Patients", "Address", c => c.String(maxLength: 255));
            AlterColumn("dbo.Patients", "Phone", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Patients", "Phone", c => c.String());
            AlterColumn("dbo.Patients", "Address", c => c.String());
            AlterColumn("dbo.Patients", "Patronymic", c => c.String());
            AlterColumn("dbo.Patients", "Surname", c => c.String());
            AlterColumn("dbo.Patients", "Name", c => c.String());
        }
    }
}
