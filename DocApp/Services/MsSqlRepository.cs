﻿using DocApp.Interfaces;
using DocApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Services
{
    public class MsSqlRepository : IRepository
    {
        public DocDb Context { get; }

        public MsSqlRepository()
        {
            this.Context = new DocDb();
        }

        #region Appointments
        public IEnumerable<Appointment> GetAppointments()
        {
            return this.Context.Appointments.ToList();
        }
        public Appointment GetAppointment(int id)
        {
            var result = this.Context.Appointments.FirstOrDefault((obj) => obj.Id == id);
            return result;
        }
        public Appointment AddAppointment(Appointment appointment)
        {
            try
            {
                var entity = this.Context.Appointments.FirstOrDefault((obj) => obj.Id == appointment.Id);
                if (entity != null) return null;
                entity = this.Context.Appointments.Add(appointment);
                this.Context.SaveChanges();
                return entity;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool EditAppointment(Appointment appointment)
        {
            try
            {
                var entity = this.Context.Appointments.FirstOrDefault((obj) => obj.Id == appointment.Id);
                if (entity == null) return false;

                bool result = entity.UpdateEntity(appointment);

                this.Context.SaveChanges();

                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool RemoveAppointment(Appointment appointment)
        {
            try
            {
                var entity = this.Context.Appointments.FirstOrDefault((obj) => obj.Id == appointment.Id);
                if (entity == null) return false;
                this.Context.Appointments.Remove(entity);
                this.Context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Patients
        public IEnumerable<Patient> GetPatients()
        {
            return this.Context.Patients.ToList();
        }
        public Patient GetPatient(int id)
        {
            var result = this.Context.Patients.FirstOrDefault((obj) => obj.Id == id);
            return result;
        }
        public Patient AddPatient(Patient patient)
        {
            try
            {
                var entity = this.Context.Patients.FirstOrDefault((obj) => obj.Id == patient.Id);
                if (entity != null) return null;
                entity = this.Context.Patients.Add(patient);
                this.Context.SaveChanges();
                return entity;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool EditPatient(Patient patient)
        {
            try
            {
                var entity = this.Context.Patients.FirstOrDefault((obj) => obj.Id == patient.Id);
                if (entity == null) return false;

                bool result = entity.UpdateEntity(patient);

                this.Context.SaveChanges();

                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool RemovePatient(Patient patient)
        {
            try
            {
                var entity = this.Context.Patients.FirstOrDefault((obj) => obj.Id == patient.Id);
                if (entity == null) return false;

                this.Context.Patients.Remove(entity);
                this.Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
