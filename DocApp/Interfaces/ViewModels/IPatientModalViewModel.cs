﻿using DocApp.Interfaces.Views;
using DocApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Interfaces.ViewModels
{
    public interface IPatientModalViewModel
    {
        IPatientModalView View { get;}
        Patient Model { get; set; }
    }
}
