﻿using DocApp.Interfaces;
using DocApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Services
{
    public class AppointmentService : IAppointmentService
    {
        private IRepository Repository { get; }
        public AppointmentService(IRepository repository)
        {
            Repository = repository;
        }


        public bool Edit(Appointment appointment)
        {
            return this.Repository.EditAppointment(appointment);
        }

        public Appointment GetAppointment(int id)
        {
            return this.Repository.GetAppointment(id);
        }

        public IEnumerable<Appointment> GetAppointments()
        {
            return this.Repository.GetAppointments();
        }

        public bool RemoveAppointment(Appointment appointment)
        {
            return this.Repository.RemoveAppointment(appointment);
        }

        public Appointment Add(Appointment appointment)
        {
            return this.Repository.AddAppointment(appointment);
        }
    }
}
