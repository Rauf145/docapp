﻿using DocApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Interfaces
{
    public interface IAppointmentService
    {
        IEnumerable<Appointment> GetAppointments();
        Appointment GetAppointment(int id);
        Appointment Add(Appointment appointment);
        bool Edit(Appointment appointment);
        bool RemoveAppointment(Appointment appointment);

    }
}
