﻿using DocApp.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Models
{
    public class Appointment : ValidationEntity<Appointment>
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public AppointmentType Type { get; set; }

        public string Diagnose { get; set; }

        public bool UpdateEntity(Appointment appointment)
        {
            try
            {
                var props = this.GetType()
                                .GetProperties(System.Reflection.BindingFlags.DeclaredOnly |
                                               System.Reflection.BindingFlags.Public |
                                               System.Reflection.BindingFlags.Instance);

                foreach (var prop in props)
                {
                    var newValue = appointment.GetType()
                                             .GetProperty(prop.Name)
                                             .GetValue(appointment);
                    prop.SetValue(this, newValue);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
