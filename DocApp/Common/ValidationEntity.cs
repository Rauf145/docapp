﻿using DocApp.Interfaces.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DocApp.Common
{
    public abstract class ValidationEntity<T> : IDataErrorInfo
    {
        private readonly Dictionary<string, Func<ValidationEntity<T>, object>> propertyGetters;
        private readonly Dictionary<string, ValidationAttribute[]> validators;

        public ValidationEntity()
        {
            this.validators = this.GetType().GetProperties()
                                .Where((p) => GetValidations(p).Length != 0)
                                .ToDictionary(p => p.Name, p => GetValidations(p));
            this.propertyGetters = this.GetType().GetProperties()
                                                 .Where((p) => GetValidations(p).Length != 0)
                                                 .ToDictionary((p) => p.Name, p => GetValueGetter(p));
        }

        public string Error
        {
            get
            {
                var errors = from validator in this.validators
                             from attribute in validator.Value
                             where !attribute.IsValid(this.propertyGetters[validator.Key](this))
                             select attribute.ErrorMessage;

                return string.Join(Environment.NewLine, errors.ToArray());
            }
        }

        private ValidationAttribute[] GetValidations(PropertyInfo property)
        {
            return property.GetCustomAttributes(typeof(ValidationAttribute), true) as ValidationAttribute[];
        }

        private Func<ValidationEntity<T>, object> GetValueGetter(PropertyInfo property)
        {
            return new Func<ValidationEntity<T>, object>((model) => property.GetValue(model, null));
        }
        public string this[string propertyName]
        {
            get
            {
                if (this.propertyGetters.ContainsKey(propertyName))
                {
                    var propertyValue = this.propertyGetters[propertyName](this);
                    var errorMessages = this.validators[propertyName]
                     .Where(v => !v.IsValid(propertyValue))
                     .Select(v => v.ErrorMessage).ToArray();

                    return string.Join(Environment.NewLine, errorMessages);
                }
                return string.Empty;
            }
        }
    }
}
